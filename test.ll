; ModuleID = 'test.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-apple-macosx10.8.0"

define i32 @main() nounwind uwtable ssp {
entry:
  %retval = alloca i32, align 4
  %x = alloca i32, align 4
  %a = alloca [1000 x i32], align 16
  %i = alloca i32, align 4
  store i32 0, i32* %retval
  store i32 4, i32* %x, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32* %i, align 4
  %cmp = icmp slt i32 %0, 1000
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32* %i, align 4
  %mul = mul nsw i32 1000, %1
  %2 = load i32* %x, align 4
  %3 = load i32* %x, align 4
  %mul1 = mul nsw i32 %2, %3
  %add = add nsw i32 %mul, %mul1
  %4 = load i32* %i, align 4
  %idxprom = sext i32 %4 to i64
  %arrayidx = getelementptr inbounds [1000 x i32]* %a, i32 0, i64 %idxprom
  store i32 %add, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %arrayidx2 = getelementptr inbounds [1000 x i32]* %a, i32 0, i64 1001
  %6 = load i32* %arrayidx2, align 4
  %7 = load i32* %x, align 4
  %div = sdiv i32 %7, 2
  %idxprom3 = sext i32 %div to i64
  %arrayidx4 = getelementptr inbounds [1000 x i32]* %a, i32 0, i64 %idxprom3
  %8 = load i32* %arrayidx4, align 4
  ret i32 0
}

define void @check_bounds(i32 %upBound, i32 %index) {
entry:
  %iLessThanBnd = icmp ult i32 %index, %upBound
  br i1 %iLessThanBnd, label %cond_return, label %cond_crash

cond_crash:                                       ; preds = %entry
  call void @exit(i32 1)
  unreachable

cond_return:                                      ; preds = %entry
  ret void
}

declare void @exit(i32)
