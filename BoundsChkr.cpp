#include "llvm/Pass.h"
#include "llvm/PassAnalysisSupport.h"
#include "llvm/Module.h"
#include "llvm/InstrTypes.h"
#include "llvm/Analysis/CallGraph.h"
#include "llvm/Function.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Analysis/Dominators.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Support/GetElementPtrTypeIterator.h"
#include "llvm/Operator.h"
#include "llvm/LLVMContext.h"
#include "llvm/Support/IRBuilder.h"
#include "llvm/DerivedTypes.h"
#include <string>
#include <set>

using namespace llvm;

namespace llvm {
    struct BoundsChkr : public ModulePass {
        static const std::string EXTERNAL_NODE;
        static const int NA;
        static char ID;
    public:
        BoundsChkr() : ModulePass(ID){ }

        virtual bool runOnModule(Module &M) {
             _insertedCount = 0;
             _Context = &(M.getContext());
            createBoundsCheckerFunction(M);
            
            
            Module::FunctionListType &funcList = M.getFunctionList();
            
            // get function stats which is most of the stats
            for (Module::iterator funci = funcList.begin(); funci != funcList.end(); ++funci) 
            {
                Function &f = *funci;
                // having no BB means it's not a function in our applicatin, it's external
                if(f.getBasicBlockList().size() > 0)
                {
                    findGEPIntructions(f);
                }
            }   
            
            errs() << "We added bounds checks to " << _insertedCount << " arrays.\n";
            return false;
        }
            
        virtual void getAnalysisUsage(AnalysisUsage &AU) const {
            // AU.addRequired<CallGraph>();
            // AU.addRequired<LoopInfo>();
            // AU.addRequired<DominatorTree>();  
        }
      
    private:
        Function* _bndschkr;  
        LLVMContext* _Context;
        int _insertedCount;
        
        // create the function that will do the bounds checks
        void createBoundsCheckerFunction(Module &M)
        {
            Constant* c = M.getOrInsertFunction("check_bounds",
                                            Type::getVoidTy(*_Context),
                                            Type::getInt64Ty(*_Context),
                                            Type::getInt64Ty(*_Context),
                                            NULL);
            // _bndschkr = cast<Function> c;
            _bndschkr = (Function*) c;
            Function::arg_iterator args = _bndschkr->arg_begin();
            Value* upBound = args++;
            upBound->setName("upBound");
            Value* index = args++;
            index->setName("index");
            
            // Three basic blocks
            BasicBlock* entry = BasicBlock::Create(*_Context, "entry", _bndschkr);
            BasicBlock* cond_crash = BasicBlock::Create(*_Context, "cond_crash", _bndschkr);
            BasicBlock* cond_return = BasicBlock::Create(*_Context, "cond_return", _bndschkr);
            
            // entry: index < upperBound 
            IRBuilder<> builder(entry);
            Value* iLessThanBnd = builder.CreateICmpULT(index, upBound, "iLessThanBnd");
            builder.CreateCondBr(iLessThanBnd, cond_return, cond_crash);
            
            // cond_crash
            builder.SetInsertPoint(cond_crash);
                //printf(credit to http://www.ibm.com/developerworks/library/os-createcompilerllvm1/) 
            llvm::Value *error = builder.CreateGlobalStringPtr("We found out of bounds :(. Exiting.\n");
            std::vector<llvm::Type *> putsArgs;
            putsArgs.push_back(builder.getInt8Ty()->getPointerTo());
            llvm::ArrayRef<llvm::Type*>  argsRef(putsArgs);
            llvm::FunctionType *putsType = 
              llvm::FunctionType::get(builder.getInt32Ty(), argsRef, false);
            llvm::Constant *putsFunc = M.getOrInsertFunction("puts", putsType);
            builder.CreateCall(putsFunc, error);
                //get Exit Function
            ArrayRef<Type*> exitArgs(Type::getInt32Ty(*_Context));
            FunctionType* exitFType = FunctionType::get(Type::getVoidTy(*_Context), exitArgs, false);
            Constant* temp = M.getOrInsertFunction("exit", exitFType);
            Function* exitF = cast<Function>(temp);
                //create the call to the exit function
            ConstantInt* one = ConstantInt::get(Type::getInt32Ty(*_Context), 1);
            builder.CreateCall(exitF, one);
            builder.CreateUnreachable();
            
            // cond_return
            builder.SetInsertPoint(cond_return);
            builder.CreateRetVoid();
        }
        
        // iterate through each BB in teh function, then iterate through
        // each instruction in that BB to find the GEP instructions.
        void findGEPIntructions(Function &f) 
        {
            // const Function::BasicBlockListType &bbList = f.getBasicBlockList();
            
            for (Function::iterator bbI = f.begin(), bbE = f.end(); bbI != bbE; ++bbI) 
            {
                for (BasicBlock::iterator insI = *bbI->begin(), insE = *bbI->end(); insI != insE; ++insI )
                {
                    if (GetElementPtrInst* GEP = dyn_cast<GetElementPtrInst>(&(*insI)) )
                    {
                        processGEP(*GEP);
                    }
                }
            }
        }
        
        void processGEP(GetElementPtrInst &GEP) {
            // errs() << " we found one with " << GEP.getNumOperands() <<" operands: " ;
            if(((PointerType*)GEP.getPointerOperandType())->getElementType()->isArrayTy() && GEP.getNumOperands() == 3)
            {
                _insertedCount++;
                IRBuilder<> builder(&GEP);
                
                // We found an array reference
                ArrayType* arrTy = (ArrayType*)((PointerType*)GEP.getPointerOperandType())->getElementType();
                // errs() << "we found an array and its of size:  " << arrTy->getNumElements();
                
                // 2nd operand should be a constant if we have a constant array array
                ConstantInt *firstOp = dyn_cast<ConstantInt>(GEP.getOperand(1));
                if (!firstOp)
                {
                    errs() << "we have no idea why we are here \n";
                    return;
                }
                // errs() << ", 2nd operand: " << firstOp->getSExtValue();
                
                // setup the call to the bounds check
                std::vector<Value*> args;
                ConstantInt* bnds = ConstantInt::get(Type::getInt64Ty(*_Context), arrTy->getNumElements());
                args.push_back(bnds);
                if (ConstantInt *index = dyn_cast<ConstantInt>(GEP.getOperand(2)))
                {
                    args.push_back(ConstantInt::get(Type::getInt64Ty(*_Context), index->getSExtValue()));
                    builder.CreateCall(_bndschkr, args);
                }
                else
                {
                    Value* indexCast = builder.CreateBitCast (GEP.getOperand(2), Type::getInt64Ty(*_Context));
                    args.push_back(indexCast);
                    builder.CreateCall(_bndschkr, args);
                }
            }
        }
    };
}

//31_llvm_bin opt -load ../../../Debug+Asserts/lib/assignment2-bernales.dylib -bndschkr < ~/Software/llvm/benchmark/mpeg2dec.bc
//gdb /Users/sergio/Software/llvm/build/Debug+Asserts/bin/opt


char BoundsChkr::ID = 0;
const int BoundsChkr::NA = -1;
const std::string BoundsChkr::EXTERNAL_NODE = "external_node__";
static RegisterPass<BoundsChkr> X("bndschkr", "Adds array bound checks to each array access.", false, false);
